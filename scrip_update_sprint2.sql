DROP TABLE if EXISTS `Action`,typeAction,statusAction;

-- -----------------------------------------------------------------------------------------------------------------------
--                           CRATION DE LA TABLE TYPE ACTION AVEC HIERARCHISATION                                                                                          
-- -----------------------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS `i_garden_db`.`typeAction` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `description` TEXT NOT NULL,
  `logo` BLOB NULL,
  `typeAction_id` BIGINT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_typeAction_typeAction1_idx` (`typeAction_id` ASC),
  CONSTRAINT `fk_typeAction_typeAction1`
    FOREIGN KEY (`typeAction_id`)
    REFERENCES `i_garden_db`.`typeAction` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------------------------------------------------------------------------
--                          CREATION DE LA TBLE STATUS ACTION                                                                                          
-- -----------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS `statusAction`;
CREATE TABLE IF NOT EXISTS `i_garden_db`.`statusAction` (
  `id` BIGINT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

DROP TABLE IF EXISTS `Action`;
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Action` (
  `id` BIGINT NOT NULL,
  `frequence` int NOT NULL,
  `dateDebut` DATE NOT NULL,
  `dateFin` DATE NULL,
  `statusAction_id` BIGINT NOT NULL,
  `utilisateur_id` BIGINT NOT NULL,
  `typeAction_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
INDEX `fk_utilisateur_utilisateur1_idx` (`utilisateur_id` ASC),
INDEX `fk_typeAction_typeAction1_idx` (`typeAction_id` ASC),
INDEX `fk_statusAction_statusAction1_idx` (`statusAction_id` ASC),
  CONSTRAINT `fk_utilisateur_utilisateur1`
    FOREIGN KEY (`utilisateur_id`)
    REFERENCES `i_garden_db`.`utilisateur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_typeAction_typeAction1`
    FOREIGN KEY (`typeAction_id`)
    REFERENCES `i_garden_db`.`typeAction` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_statusAction_statusAction1`
    FOREIGN KEY (`statusAction_id`)
    REFERENCES `i_garden_db`.`statusAction` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------------------------------------------------------------------------
--                                         CREATION DE LA TABLE CONTENANT LES NOTES DE L'UTILISATEUR                                                                           
-- -----------------------------------------------------------------------------------------------------------------------

DROP TABLE if EXISTS notes;
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Notes` (
  `id` BIGINT NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `lienAudio` VARCHAR(255) NULL,
  `dateCreation` DATE NULL,
  `zone_id` BIGINT NULL,
  `plante_id` BIGINT NULL,
  `graine_id` BIGINT NULL,
  PRIMARY KEY (`id`),
INDEX `fk_zone_zone1_idx` (`zone_id` ASC),
INDEX `fk_plante_plante1_idx` (`plante_id` ASC),
INDEX `fk_graine_graine1_idx` (`graine_id` ASC),
  CONSTRAINT `fk_zone_zone1`
    FOREIGN KEY (`zone_id`)
    REFERENCES `i_garden_db`.`zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_plante_plante1`
    FOREIGN KEY (`plante_id`)
    REFERENCES `i_garden_db`.`plante` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_graine_graine1`
    FOREIGN KEY (`graine_id`)
    REFERENCES `i_garden_db`.`graine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
    )
ENGINE = InnoDB;

-- -----------------------------------------------------------------------------------------------------------------------
--       MODIFICATION DE LA TABLE ZONE POUR AJOUTER LA LATITUDE LONGITUDE LARGEUR ET HAUTEUR                                                                        
-- -----------------------------------------------------------------------------------------------------------------------

ALTER TABLE zone 
ADD COLUMN `latitude` DECIMAL(8,6) NOT NULL,
ADD COLUMN `longitude` DECIMAL(8,6) NOT NULL,
ADD COLUMN `largeur` int NOT NULL,
ADD COLUMN `hauteur` int NOT NULL;

-- -----------------------------------------------------------------------------------------------------------------------
--                                 AJOUT D'UNE TEMPERATURE MINIMALE POUR LES PLANTES                                                                                    
-- -----------------------------------------------------------------------------------------------------------------------

ALTER TABLE plante 
ADD COLUMN `minTemperature` int NOT NULL;

-- -----------------------------------------------------------------------------------------------------------------------
--                       CREATION AVEC TRANSFERT DES DONNEES ACTUELLES DE LA TABLE PLANTE                                                                        
-- -----------------------------------------------------------------------------------------------------------------------

DROP TABLE if EXISTS nomDeLaPlante;
SELECT * from plante LIMIT 10000000000000 FOR UPDATE;
CREATE TABLE nomdelaplante (id BIGINT NOT NULL AUTO_INCREMENT, nom VARCHAR(255) NOT NULL, plante_id BIGINT NULL,  PRIMARY KEY (`id`)) AS 
SELECT NULL, g.nom,plante.id FROM plante INNER JOIN graine g ON plante.Graine_id=g.id;
UNLOCK TABLES;

