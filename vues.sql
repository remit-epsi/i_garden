USE `i_garden_db` ;
-- --------------------------------------------
-- Liste des plantes
-- --------------------------------------------
CREATE VIEW listedesplantes
AS
SELECT
    p.id,
    g.nom AS nomGraine,
    z.nom AS nomZone ,
    cv.nom AS nomCycle
FROM
    `plante` p
JOIN graine g ON
    g.id = p.Graine_id
JOIN zonesdelaplante zp ON
    zp.Plante_id = p.id
JOIN zone z ON
    z.id = zp.Zone_id
JOIN plantesetsescycles pc ON
    pc.Plante_id = p.id
JOIN cyclevegetatif cv ON
    cv.id = pc.CycleVegetatif_id
;	
-- --------------------------------------------
-- Liste des plantes hors zones
-- --------------------------------------------

CREATE VIEW planteshorszones
AS
SELECT
    p.id,
    g.nom,
    zp.Zone_id
FROM
    `plante` p
JOIN graine g ON
    g.id = p.Graine_id
JOIN plantesposssedeesparutilisateur pu ON
    pu.Plante_id = p.id
JOIN utilisateur u ON
    u.id = pu.Utilisateur_id
JOIN zonesdelaplante zp ON
    zp.Plante_id = p.id
WHERE
    zp.Zone_id IS NULL
;
-- --------------------------------------------
-- Liste des zones
-- --------------------------------------------
CREATE VIEW zonesrecap
AS
SELECT
    z.nom,
    c.zoneDeRusticite,
    COUNT(zp.Plante_id)
FROM
    `zone` z
JOIN criterederusticite c ON
    c.id = z.CritereDeRusticite_id
JOIN zonesdelaplante zp ON
    zp.Zone_id = z.id
WHERE
    z.id = zp.Zone_id
;	
-- --------------------------------------------
-- Liste des plantes et classification
-- --------------------------------------------
CREATE VIEW plantesrecap
AS
SELECT
    g.nom AS nomGraine,
    g.nomLatin,
    c.nom AS nomClassification
FROM
    `graine` g
JOIN classification c ON
    c.id = g.Classification_id
;	
-- --------------------------------------------
-- Creation index classification
-- --------------------------------------------
ALTER TABLE `classification`
ROW_FORMAT = DYNAMIC;
CREATE INDEX `classificationfamille` ON `classification` (`nom`);	
-- --------------------------------------------
-- Modification de critere de rusticite
-- --------------------------------------------
ALTER TABLE `criterederusticite` CHANGE `celsius` `celsiusMin` 
FLOAT NULL DEFAULT NULL;
-- --------------------------------------------
-- Ajout d'une colonne a critere de rusticite
-- --------------------------------------------
ALTER TABLE `criterederusticite` ADD `celsiusMax` 
FLOAT NULL ;
-- --------------------------------------------
-- Modification du type zone de rusticite
-- --------------------------------------------
ALTER TABLE `criterederusticite` CHANGE `zoneDeRusticite` 
`zoneDeRusticite` VARCHAR(255) NULL DEFAULT NULL;
