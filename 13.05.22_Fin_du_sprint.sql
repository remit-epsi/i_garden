-- --------------------------------------------
-- Trigger sur santedelaplante
-- --------------------------------------------
DELIMITER |
create trigger on_etat_update
    AFTER update
    on santedelaplante
    for each row
    IF NEW.Etat_id != OLD.Etat_id THEN
        INSERT INTO santedelaplante (`Etat_id`,`Plante_id`,`Statut_id`,`date`) VALUES (NEW.etat_id, NEW.plante_id, NEW.statut_id,CURDATE());
    END IF;
|
DELIMITER ;

-- --------------------------------------------
-- Trigger sur plantesetsescycles
-- --------------------------------------------

DELIMITER |
create trigger on_update_cycle_vegetatif
    AFTER update
    on plantesetsescycles
    for each row
     IF NEW.CycleVegetatif_id != OLD.CycleVegetatif_id THEN
        INSERT INTO plantesetsescycles (`CycleVegetatif_id`,`Plante_id`,`dateChangement`) VALUES (NEW.CycleVegetatif_id, NEW.plante_id, CURDATE());
    END IF;
|
DELIMITER ;

-- --------------------------------------------
-- Trigger sur zonesdelaplante
-- --------------------------------------------

DELIMITER |
create trigger on_update_zones_de_la_plante
    AFTER update
    on zonesdelaplante
    for each row
     IF NEW.Zone_id != OLD.Zone_id THEN
        INSERT INTO zonesdelaplante (`Zone_id`,`Plante_id`,`date`) VALUES (NEW.Zone_id, NEW.plante_id, CURDATE());
    END IF;
|
DELIMITER ;

-- --------------------------------------------
-- Rectification erreur ON DELETE
-- --------------------------------------------

ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_CycleVegetatif1`;
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_CycleVegetatif1` FOREIGN KEY (`id_cyle`) REFERENCES `cyclevegetatif`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION; 
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_Graine1`; 
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_Graine1` FOREIGN KEY (`Graine_id`) REFERENCES `graine`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION; 
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_Jardin1`; 
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_Jardin1` FOREIGN KEY (`id_jardin`) REFERENCES `cyclevegetatif`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION; 
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_Zone1`; 
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_Zone1` FOREIGN KEY (`id_zone`) REFERENCES `zone`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --------------------------------------------
-- ON DELETE CASCADE sur les historiques
-- --------------------------------------------

ALTER TABLE `plantesetsescycles` DROP FOREIGN KEY `fk_CycleVegetatif_has_Plante_Plante1`;
ALTER TABLE `plantesetsescycles` ADD CONSTRAINT `fk_CycleVegetatif_has_Plante_Plante1` FOREIGN KEY (`Plante_id`) REFERENCES `plante`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE `santedelaplante` DROP FOREIGN KEY `fk_Etat_has_Plante_Plante1`;
ALTER TABLE `santedelaplante` ADD CONSTRAINT `fk_Etat_has_Plante_Plante1` FOREIGN KEY (`Plante_id`) REFERENCES `plante`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
ALTER TABLE `zonesdelaplante` DROP FOREIGN KEY `fk_Zone_has_Plante_Plante1`;
ALTER TABLE `zonesdelaplante` ADD CONSTRAINT `fk_Zone_has_Plante_Plante1` FOREIGN KEY (`Plante_id`) REFERENCES `plante`(`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

-- --------------------------------------------
-- Liste des plantes hors zone modification
-- --------------------------------------------

CREATE VIEW planteshorszonesbis
AS
SELECT
    p.id,
    g.nom,
    p.id_zone
FROM
    `plante` p
JOIN graine g ON
    g.id = p.Graine_id
WHERE
    p.id_zone IS NULL
;