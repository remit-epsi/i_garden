USE `i_garden_db` ;

-- --------------------------------------------
-- Modifiation de tables
-- --------------------------------------------

ALTER TABLE cyclevegetatif
ADD dateChangement DATE;

ALTER TABLE plantesetsescycles
ADD dateChangement DATE;

ALTER TABLE plante
ADD id_zone BIGINT;
ALTER TABLE plante
ADD id_cyle BIGINT;
ALTER TABLE plante
ADD id_jardin BIGINT;

ALTER TABLE plante
ADD CONSTRAINT `fk_Plante_Zone1`
    FOREIGN KEY (`id_zone`)
    REFERENCES `i_garden_db`.`Zone` (`id`);

ALTER TABLE plante
ADD CONSTRAINT `fk_Plante_CycleVegetatif1`
    FOREIGN KEY (`id_cyle`)
    REFERENCES `i_garden_db`.`CycleVegetatif` (`id`);

ALTER TABLE plante
ADD CONSTRAINT `fk_Plante_Jardin1`
    FOREIGN KEY (`id_jardin`)
    REFERENCES `i_garden_db`.`CycleVegetatif` (`id`);


-- --------------------------------------------
-- Modifiation pour le DELETE ON CASCADE de plante
-- --------------------------------------------	
	
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_CycleVegetatif1`;
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_CycleVegetatif1` FOREIGN KEY (`id_cyle`) REFERENCES `cyclevegetatif`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_Graine1`;
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_Graine1` FOREIGN KEY (`Graine_id`) REFERENCES `graine`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_Jardin1`;
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_Jardin1` FOREIGN KEY (`id_jardin`) REFERENCES `cyclevegetatif`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `plante` DROP FOREIGN KEY `fk_Plante_Zone1`;
ALTER TABLE `plante` ADD CONSTRAINT `fk_Plante_Zone1` FOREIGN KEY (`id_zone`) REFERENCES `zone`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- --------------------------------------------
-- Liste des plantes
-- --------------------------------------------
CREATE VIEW listedesplantesbis
AS
SELECT
    p.id,
    g.nom AS nomGraine,
    z.nom AS nomZone ,
    cv.nom AS nomCycle
FROM
    `plante` p
JOIN graine g ON
    g.id = p.Graine_id
JOIN zone z ON
    z.id = p.id_zone
JOIN cyclevegetatif cv ON
    cv.id = p.id_cyle
;



	


