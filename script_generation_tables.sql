-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema i_garden_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema i_garden_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `i_garden_db` DEFAULT CHARACTER SET utf8mb4 ;
USE `i_garden_db` ;

-- -----------------------------------------------------
-- Table `i_garden_db`.`Etat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Etat` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `date` DATE NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Statut`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Statut` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Type` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Classification`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Classification` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `Classification_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Classification_Classification1_idx` (`Classification_id` ASC),
  CONSTRAINT `fk_Classification_Classification1`
    FOREIGN KEY (`Classification_id`)
    REFERENCES `i_garden_db`.`Classification` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Graine`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Graine` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `nomLatin` VARCHAR(255) NULL,
  `hauteurMaximale` FLOAT NULL,
  `photo` BLOB NULL,
  `icone` BLOB NULL,
  `description` LONGTEXT NULL,
  `Type_id` BIGINT NOT NULL,
  `Classification_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Graine_Type1_idx` (`Type_id` ASC),
  INDEX `fk_Graine_Classification1_idx` (`Classification_id` ASC),
  CONSTRAINT `fk_Graine_Type1`
    FOREIGN KEY (`Type_id`)
    REFERENCES `i_garden_db`.`Type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Graine_Classification1`
    FOREIGN KEY (`Classification_id`)
    REFERENCES `i_garden_db`.`Classification` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Utilisateur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Utilisateur` (
  `id` BIGINT NOT NULL,
  `pseudo` VARCHAR(32) NOT NULL,
  `mail` VARCHAR(255) NOT NULL,
  `motDePasse` VARCHAR(32) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Plante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Plante` (
  `id` BIGINT NOT NULL,
  `dateDuSemis` DATE NULL,
  `dateDuRepiquage` DATE NULL,
  `dateDeFloraison` DATE NULL,
  `quantiteRecoltee` INT(64) NULL,
  `Graine_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Plante_Graine1_idx` (`Graine_id` ASC),
  CONSTRAINT `fk_Plante_Graine1`
    FOREIGN KEY (`Graine_id`)
    REFERENCES `i_garden_db`.`Graine` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`CritereDeRusticite`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`CritereDeRusticite` (
  `id` BIGINT NOT NULL,
  `zoneDeRusticite` INT(2) NULL,
  `celsius` FLOAT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`CodePostal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`CodePostal` (
  `id` BIGINT NOT NULL,
  `numero` DECIMAL(5) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Jardin`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Jardin` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `CritereDeRusticite_id` BIGINT NOT NULL,
  `CodePostal_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Jardin_CritereDeRusticite1_idx` (`CritereDeRusticite_id` ASC),
  INDEX `fk_Jardin_CodePostal1_idx` (`CodePostal_id` ASC),
  CONSTRAINT `fk_Jardin_CritereDeRusticite1`
    FOREIGN KEY (`CritereDeRusticite_id`)
    REFERENCES `i_garden_db`.`CritereDeRusticite` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Jardin_CodePostal1`
    FOREIGN KEY (`CodePostal_id`)
    REFERENCES `i_garden_db`.`CodePostal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`Zone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`Zone` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  `typeDeSol` VARCHAR(255) NULL,
  `description` LONGTEXT NULL,
  `CritereDeRusticite_id` BIGINT NOT NULL,
  `Jardin_id` BIGINT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Zone_CritereDeRusticite1_idx` (`CritereDeRusticite_id` ASC),
  INDEX `fk_Zone_Jardin1_idx` (`Jardin_id` ASC),
  CONSTRAINT `fk_Zone_CritereDeRusticite1`
    FOREIGN KEY (`CritereDeRusticite_id`)
    REFERENCES `i_garden_db`.`CritereDeRusticite` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Zone_Jardin1`
    FOREIGN KEY (`Jardin_id`)
    REFERENCES `i_garden_db`.`Jardin` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`CycleVegetatif`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`CycleVegetatif` (
  `id` BIGINT NOT NULL,
  `nom` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`PlantesEtSesCycles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`PlantesEtSesCycles` (
  `CycleVegetatif_id` BIGINT NOT NULL,
  `Plante_id` BIGINT NOT NULL,
  PRIMARY KEY (`CycleVegetatif_id`, `Plante_id`),
  INDEX `fk_CycleVegetatif_has_Plante_Plante1_idx` (`Plante_id` ASC),
  INDEX `fk_CycleVegetatif_has_Plante_CycleVegetatif_idx` (`CycleVegetatif_id` ASC),
  CONSTRAINT `fk_CycleVegetatif_has_Plante_CycleVegetatif`
    FOREIGN KEY (`CycleVegetatif_id`)
    REFERENCES `i_garden_db`.`CycleVegetatif` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_CycleVegetatif_has_Plante_Plante1`
    FOREIGN KEY (`Plante_id`)
    REFERENCES `i_garden_db`.`Plante` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`JardinsPossedesParUtilisateurs`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`JardinsPossedesParUtilisateurs` (
  `Utilisateur_id` BIGINT NOT NULL,
  `Jardin_id` BIGINT NOT NULL,
  PRIMARY KEY (`Utilisateur_id`, `Jardin_id`),
  INDEX `fk_Utilisateur_has_Jardin_Jardin1_idx` (`Jardin_id` ASC),
  INDEX `fk_Utilisateur_has_Jardin_Utilisateur1_idx` (`Utilisateur_id` ASC),
  CONSTRAINT `fk_Utilisateur_has_Jardin_Utilisateur1`
    FOREIGN KEY (`Utilisateur_id`)
    REFERENCES `i_garden_db`.`Utilisateur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Utilisateur_has_Jardin_Jardin1`
    FOREIGN KEY (`Jardin_id`)
    REFERENCES `i_garden_db`.`Jardin` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`PlantesPosssedeesParUtilisateur`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`PlantesPosssedeesParUtilisateur` (
  `Plante_id` BIGINT NOT NULL,
  `Utilisateur_id` BIGINT NOT NULL,
  PRIMARY KEY (`Plante_id`, `Utilisateur_id`),
  INDEX `fk_Plante_has_Utilisateur_Utilisateur1_idx` (`Utilisateur_id` ASC),
  INDEX `fk_Plante_has_Utilisateur_Plante1_idx` (`Plante_id` ASC),
  CONSTRAINT `fk_Plante_has_Utilisateur_Plante1`
    FOREIGN KEY (`Plante_id`)
    REFERENCES `i_garden_db`.`Plante` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Plante_has_Utilisateur_Utilisateur1`
    FOREIGN KEY (`Utilisateur_id`)
    REFERENCES `i_garden_db`.`Utilisateur` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`SanteDeLaPlante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`SanteDeLaPlante` (
  `Etat_id` BIGINT NOT NULL,
  `Plante_id` BIGINT NOT NULL,
  `Statut_id` BIGINT NOT NULL,
  `date` DATE NULL,
  PRIMARY KEY (`Etat_id`, `Plante_id`, `Statut_id`),
  INDEX `fk_Etat_has_Plante_Plante1_idx` (`Plante_id` ASC),
  INDEX `fk_Etat_has_Plante_Etat1_idx` (`Etat_id` ASC),
  INDEX `fk_SanteDeLaPlante_Statut1_idx` (`Statut_id` ASC),
  CONSTRAINT `fk_Etat_has_Plante_Etat1`
    FOREIGN KEY (`Etat_id`)
    REFERENCES `i_garden_db`.`Etat` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Etat_has_Plante_Plante1`
    FOREIGN KEY (`Plante_id`)
    REFERENCES `i_garden_db`.`Plante` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_SanteDeLaPlante_Statut1`
    FOREIGN KEY (`Statut_id`)
    REFERENCES `i_garden_db`.`Statut` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `i_garden_db`.`ZonesDeLaPlante`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `i_garden_db`.`ZonesDeLaPlante` (
  `Zone_id` BIGINT NOT NULL,
  `Plante_id` BIGINT NOT NULL,
  `date` DATE NULL,
  PRIMARY KEY (`Zone_id`, `Plante_id`),
  INDEX `fk_Zone_has_Plante_Plante1_idx` (`Plante_id` ASC),
  INDEX `fk_Zone_has_Plante_Zone1_idx` (`Zone_id` ASC),
  CONSTRAINT `fk_Zone_has_Plante_Zone1`
    FOREIGN KEY (`Zone_id`)
    REFERENCES `i_garden_db`.`Zone` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Zone_has_Plante_Plante1`
    FOREIGN KEY (`Plante_id`)
    REFERENCES `i_garden_db`.`Plante` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
